import { IUser } from './user.interface';
import { ADD_USER, REMOVE_ALL_USERS } from './actions';

export interface IAppState {
  users: Array<IUser>;
  lastUpdate: Date;
}

export const INITIAL_STATE: IAppState = {
  users: [],
  lastUpdate: null
}

export function rootReducer(state: IAppState, action): IAppState {

  switch(action.type) {

    case ADD_USER:
    action.user.id = state.users.length + 1;
      return Object.assign({}, state, {
        users: state.users.concat(Object.assign({}, action.user)),
        lastUpdate: new Date()
      });
    
    case REMOVE_ALL_USERS:
      return Object.assign({}, state, {
        users: [],
        lastUpdate: new Date()
      });

    default:
    return state;
      
  }
}