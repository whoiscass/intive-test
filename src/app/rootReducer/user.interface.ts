export interface IUser {
  id: number;
  name: String;
  country: String;
  msg: String
}