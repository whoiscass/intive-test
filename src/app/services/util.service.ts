import { Injectable } from '@angular/core';
import * as moment from 'moment';
declare const Push: any;

@Injectable()
export class UtilService {

  constructor() { }

  getDate(e: any) {
    let fecha = new Date(e.target.valueAsDate);
    let hoy = new Date();
    let age = (hoy.getUTCFullYear() - fecha.getUTCFullYear());

    if((age) == 0) {
      return  ('El proximo dia ' + fecha.getUTCDate() + 
      ' del mes ' + (fecha.getUTCMonth() + 1)  + 
      ' tendras ' + (hoy.getUTCFullYear() - 
      fecha.getUTCFullYear() + 1) + ' años');

    }else if((age) > 0 && fecha.getUTCDate() < hoy.getUTCDate()) {
      return  ('El proximo dia ' + fecha.getUTCDate() + 
      ' del mes ' + (fecha.getUTCMonth() + 1)  + 
      ' tendras ' + (hoy.getUTCFullYear() - 
      fecha.getUTCFullYear() + 1) + ' años');

    }else if(age > 0 && fecha.getUTCDate() > hoy.getUTCDate()) {
      return  ('El proximo dia ' + fecha.getUTCDate() + 
      ' del mes ' + (fecha.getUTCMonth() + 1)  + 
      ' tendras ' + (hoy.getUTCFullYear() - 
      fecha.getUTCFullYear()) + ' años');

    }else if(age > 0 && fecha.getUTCDate() == hoy.getUTCDate()) {
      return  ('Feliz cumpleaños, numero ' + age);
    }

    return ('El proximo dia ' + fecha.getUTCDate() + 
    ' del mes ' + (fecha.getUTCMonth() + 1)  + 
    ' tendras ' + (hoy.getUTCFullYear() - 
    fecha.getUTCFullYear() + 1) + ' años');
  }

  greet(i) {
    Push.create(`Hola ${i.name}`, {
      body: i.msg,
      timeout: 4000,
      onClick: function () {
          window.focus();
          this.close();
      }
    });
  }

}
