import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpService {

  constructor(private http: HttpClient) { }

  getContries() {
    return this.http.get('https://restcountries.eu/rest/v2/all')
    .map((paises: any[]) => {
      return paises;
    });
  }

}
