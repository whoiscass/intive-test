import { Component, OnInit } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store';

import { HttpService } from './services/http.service';
import { UtilService } from './services/util.service';
import { IAppState } from './rootReducer/store';
import{ IUser } from './rootReducer/user.interface';
import { ADD_USER, REMOVE_ALL_USERS } from './rootReducer/actions';

declare const Push: any;
declare const $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  warning: String = 'La fecha seleccionada no es valida';
  msg: String;
  isValid: boolean = true;
  validUser: boolean = true;
  loaded: boolean = false;
  countries: Array<String> = [];
  user: IUser = {
    id: 0,
    name: null,
    country: null,
    msg: null
  };

  @select('users') users;
  @select('lastUpdate') lastUpdate;

  constructor(private http: HttpService, private ngRedux: NgRedux<IAppState>, private util: UtilService) {}

  ngOnInit() {
    this.http.getContries()
    .subscribe((c: Array<any>) => {
      c.map((i: any) => {
        this.countries.push(i.name);
        if(this.countries !== []) this.loaded = true;
      });
    });
  }

  newUser() {
    if(this.user.name == null || this.user.country == null) {
      this.validUser = false;
    }else {
      this.validUser = true;
      this.user.msg = this.msg;
      this.ngRedux.dispatch({type: ADD_USER, user: this.user});
      this.util.greet(this.user);
      this.user.name = null;
      this.user.country = null;
    }
  }

  greet(e: any) {
    this.isValid = true;
    let now = new Date();
    let bd = new Date(e.target.valueAsDate);
    if(bd > now) {
      this.isValid = false;
    }else {
      this.msg = this.util.getDate(e);
    }
  }


}
