import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgRedux, NgReduxModule } from '@angular-redux/store';


//SERVICES
import { HttpService } from './services/http.service';
import { UtilService } from './services/util.service';
import { IAppState, rootReducer, INITIAL_STATE } from './rootReducer/store';

//COMPONENTS
import { AppComponent } from './app.component';
import { UserOverviewComponent } from './user-overview/user-overview.component';

@NgModule({
  declarations: [
    AppComponent,
    UserOverviewComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    NgReduxModule
  ],
  providers: [HttpService, UtilService],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(ngRedux: NgRedux<IAppState>) {
    ngRedux.configureStore(rootReducer, INITIAL_STATE);
  }
}
