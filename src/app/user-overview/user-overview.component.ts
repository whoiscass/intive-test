import { Component, OnInit } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store';
import { UtilService } from '../services/util.service';
declare const Push: any;

@Component({
  selector: 'app-user-overview',
  templateUrl: './user-overview.component.html',
  styleUrls: ['./user-overview.component.css']
})
export class UserOverviewComponent implements OnInit {

  @select('users') users;
  @select('lastUpdate') lastUpdate;

  constructor(private util: UtilService) { }

  ngOnInit() {
  }

  hello(i) {
    this.util.greet(i);
  }

}
